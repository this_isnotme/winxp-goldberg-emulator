cd /d "%~dp0"
del /Q /S *.obj
del /Q /S rtlgenrandom.exe
del /Q /S rtlgenrandom.exp
del /Q /S rtlgenrandom.lib
del /Q /S base.exp
del /Q /S base.lib
del /Q /S release\*
del /Q release\lobby_connect\*
del /Q /S release\lobby_connect\*.lib
del /Q /S release\lobby_connect\*.exp
del /Q release\tools\*
del /Q /S release\tools\*.lib
del /Q /S release\tools\*.exp
rmdir /S /Q release\experimental
rmdir /S /Q release\experimental_steamclient
rmdir /S /Q release\lobby_connect
rmdir /S /Q release
