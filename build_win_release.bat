@echo off
cd /d "%~dp0"
del /Q /S release\*
rmdir /S /Q release\experimental
rmdir /S /Q release\experimental_steamclient
rmdir /S /Q release\lobby_connect
rmdir /S /Q release
mkdir release
call build_set_protobuf_directories.bat
"%PROTOC_X86_EXE%" -I.\dll\ --cpp_out=.\dll\ .\dll\net.proto
call build_env_x86xp.bat

set INCLUDE=%ProgramFiles(x86)%\Microsoft SDKs\Windows\7.1A\Include;%INCLUDE%

set PATH="C:\Program Files (x86)\Microsoft SDKs\Windows\7.1A\Bin";%PATH%

set LIB=%ProgramFiles(x86)%\Microsoft SDKs\Windows\v7.1A\Lib;%LIB%

set CL=/D_USING_V110_SDK71_;%CL%
cl /arch:SSE dll/rtlgenrandom.c dll/rtlgenrandom.def
cl /arch:SSE /LD /DEMU_RELEASE_BUILD /DNDEBUG /I%PROTOBUF_X86_DIRECTORY%\include\ dll/*.cpp dll/*.cc "%PROTOBUF_X86_LIBRARY%" Iphlpapi.lib Ws2_32.lib rtlgenrandom.lib Shell32.lib /EHsc /MP12 /Ox /D "_USING_V110_SDK71_" /link /LIBPATH:"C:\Program Files (x86)\Microsoft SDKs\Windows\v7.1A\Lib" /debug:none /OUT:release\steam_api.dll

copy Readme_release.txt release\Readme.txt
xcopy /s files_example\* release\
call build_win_lobby_connect.bat
call build_win_find_interfaces.bat
