@ecbo off
cd /d "%~dp0"
mkdir release\tools
del /Q release\tools\*
call build_env_x86.bat
set CL=/D_USING_V110_SDK71_;%CL%
set LINK=/SUBSYSTEM:CONSOLE,5.01 %LINK%
cl generate_interfaces_file.cpp /arch:SSE /EHsc /MP12 /Ox /link /debug:none /OUT:release\tools\generate_interfaces_file.exe
del /Q /S release\tools\*.lib
del /Q /S release\tools\*.exp
copy Readme_generate_interfaces.txt release\tools\Readme_generate_interfaces.txt
