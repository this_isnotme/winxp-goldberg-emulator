@echo off
cd /d "%~dp0"
rem Put in the base path in which Visual Studio is installed, default would be C:\Program Files (x86)

set VS_Base_Path=C:\Program Files (x86)

if exist "%VS_Base_Path%\Microsoft Visual Studio 12.0\VC\bin\amd64_x86\vcvarsamd64_x86.bat" goto vs12

:vs12
call "%VS_Base_Path%\Microsoft Visual Studio 12.0\VC\bin\amd64_x86\vcvarsamd64_x86.bat"
goto batend

:batend
